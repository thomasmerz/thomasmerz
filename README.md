### Welcome to my GitLab Profile!

- Married, dad of three boys
- 🐧 Linux since 1994
- 🚫 No Windows please!
- Some weird "hobbies":
  - speed-solving Rubik's Cubes (3x3, 5x5, 7x7, 9x9, 13x13)
  - HEAVY METAL!
  - Everything with Star Wars…
  - Lego :-)

![My GitLab stats](https://gitlab-readme-stats.vercel.app/api?username=thomasmerz&show_icons=true)  
![My GitHub stats](https://github-readme-stats.vercel.app/api?username=thomasmerz&show_icons=true&theme=dracula)
